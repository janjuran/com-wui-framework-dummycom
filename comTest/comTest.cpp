﻿// comTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>

#define LOAD_FROM_CLSID	0

#ifdef _DEBUG
#import "..\DummyCom\Debug\DummyCOM.dll"
#else
#import "..\DummyCom\Relese\DummyCOM.dll"
#endif

typedef unsigned char Boolean;

using namespace std;

int main()
{
	
#if LOAD_FROM_CLSID
	string $clsid = "{09c58a02-4ca6-4822-91fe-075242e3f70c}";
	string errorMsg;
	int instanceID = -1;
	
	if (!SUCCEEDED(CoInitializeEx(NULL, COINIT_MULTITHREADED))) {
		cout << "Failed to initialize COM interface" << endl;
		return 1;
	}

	CLSID clsid{};
	//HRESULT res = CLSIDFromString(stringToWstring($clsid).c_str(), &clsid);
	wstring wclsid($clsid.begin(), $clsid.end());
	HRESULT res = CLSIDFromString(wclsid.c_str(), &clsid);

	if (SUCCEEDED(res)) {
		IUnknown *instance = nullptr;
		if (S_OK != CoCreateInstance(clsid, NULL, CLSCTX_ALL,
			IID_IUnknown, reinterpret_cast<void **>(&instance))) {
			cout << "Instance failed to create:\t(" + $clsid + ")" << endl;			
			return 2;
		}		
	}
	else {
		cout << "Some other error" << endl;
		return 3;
	}
#else
	CoInitialize(NULL);
	WuiFrameworkDummyCOM::IDummyPtr pFastAddAlgorithm;
	pFastAddAlgorithm.CreateInstance("WuiFramework.DummyCOM");	
	if (!pFastAddAlgorithm) {
		cout << "COM not loaded..." << endl;
		return 4;
	}

	cout << "Tests: " << endl;
	pFastAddAlgorithm->CallWithEmpty();
	cout << pFastAddAlgorithm->ReturnBool() << endl;
	cout << pFastAddAlgorithm->ReturnInt() << endl;	
	cout << pFastAddAlgorithm->ReturnFloat() << endl;
	cout << pFastAddAlgorithm->ReturnDouble() << endl;

	pFastAddAlgorithm->CallWithInt(13);
	pFastAddAlgorithm->CallWithBool(true);
	pFastAddAlgorithm->CallWithFloat(2.16f);
	pFastAddAlgorithm->CallWithDouble(3.14);	
	char str[16];
	strcpy_s(str, "Wenger out!");
	pFastAddAlgorithm->CallWithString(str); /* for some reson even though the interface is defined with const, this does not work with const char*/

	cout << pFastAddAlgorithm->int_func_bool_int(true, 42) << endl;
	cout << pFastAddAlgorithm->int_func_int_bool(43, false) << endl;
	cout << pFastAddAlgorithm->add_input_ints(44, 45) << endl;
	
	int x = 7;
	cout << pFastAddAlgorithm->output_args(&x) << " x: " << x << endl;
	cout << pFastAddAlgorithm->output_args_char(/*[out]*/ str) << "; str: " << str << endl;
	float f = 11.4f;
	pFastAddAlgorithm->output_args_float(&f); cout << "f: " << f << endl;
	double d = 112.16;
	pFastAddAlgorithm->output_args_double(&d); cout << "d: " << d << endl;
	
	char b = TRUE; /* for some reason, from here it sees the value as signed char, although that in the COM implementation, it is boolean, which is unsigned char */	
	cout << pFastAddAlgorithm->output_args_bool(&b) << "; b: " << (bool) b << endl;
	//cout << pFastAddAlgorithm->output_args_int_bool_float(&x, &g, &f) << "; x: " << x << "; b: " << b << "; f: " << f << end;
	//cout << pFastAddAlgorithm->input_int_double_output_bool_int_input_string(/*[in]*/ int inI, /*[in]*/ float inD, /*[out]*/ BOOL *b, /*[out]*/ int *i, /*[in]*/ const signed char *inC, /*[out, retval]*/ int* result);


#endif

    return 0;
}

