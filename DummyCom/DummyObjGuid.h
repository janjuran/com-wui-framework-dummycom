//
//DummyObjGuid.h
//
#ifndef __DummyObjGuid_h__
#define __DummyObjGuid_h__

// {09c58a02-4ca6-4822-91fe-075242e3f70c}
static const GUID CLSID_DummyObject =
{ 0x09c58a02, 0x4ca6, 0x4822,{ 0x91, 0xfe, 0x07, 0x52, 0x42, 0xe3, 0xf7, 0x0c } };

#endif
