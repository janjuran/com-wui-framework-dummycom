//
//AddObj.cpp
//Contains the C++ class body for implementing the IDummy, IFileIO
//interfaces
//


#include    <objbase.h>

#include    "DummyObj.h"
#include    "IDummy_i.c"
#include	<cstdio>

HRESULT __stdcall CDummyObj::CallWithEmpty()
{
	printf("DLL: %s invoked\n", __FUNCTION__);
	return S_OK;
}

HRESULT CDummyObj::CallWithInt(int i)
{
	printf("DLL: %s invoked with: %d\n", __FUNCTION__, i);
	return S_OK;
}

HRESULT CDummyObj::CallWithBool(BOOL start)
{
	printf("DLL: %s invoked with: %s\n", __FUNCTION__, start ? "true" : "false");
	return S_OK;
}

HRESULT CDummyObj::CallWithFloat(float f)
{
	printf("DLL: %s invoked with: %.3f\n", __FUNCTION__, f);
	return S_OK;
}

HRESULT CDummyObj::CallWithDouble(double d)
{
	printf("DLL: %s invoked with: %.3f\n", __FUNCTION__, d);
	return S_OK;
}

HRESULT CDummyObj::CallWithString(const signed char * project)
{
	printf("DLL: %s invoked with: %s\n", __FUNCTION__, project);
	return S_OK;
}

HRESULT CDummyObj::ReturnInt(int * result){
	printf("DLL: %s invoked\n", __FUNCTION__);
	*result = 23;
	return S_OK;
}

HRESULT CDummyObj::ReturnBool(BOOL * result){
	printf("DLL: %s invoked\n", __FUNCTION__);
	*result = true;
	return S_OK;
}

HRESULT CDummyObj::ReturnFloat(float * result){
	printf("DLL: %s invoked\n", __FUNCTION__);
	*result = 16.3f;
	return S_OK;
}

HRESULT CDummyObj::ReturnDouble(double * result){
	printf("DLL: %s invoked\n", __FUNCTION__);
	*result = 33.44;
	return S_OK;
}

HRESULT CDummyObj::int_func_bool_int(BOOL b, int i, int * result)
{
	printf("DLL: %s invoked with: %s and %d\n", __FUNCTION__, b ? "true" : "false", i);
	*result = 23;
	return S_OK;
}

HRESULT	CDummyObj::int_func_int_bool(int i, BOOL b, int* result) {
	printf("DLL: %s invoked with: %d and %s\n", __FUNCTION__, i, b ? "true" : "false");
	*result = 29;
	return S_OK;
}


HRESULT CDummyObj::add_input_ints(int a, int b, int* result)
{
	printf("DLL: %s invoked with: %d and %d\n", __FUNCTION__, a, b);
	*result = a + b;
	return S_OK;
}

HRESULT CDummyObj::output_args(int * a)
{
	int b = *a;
	printf("DLL: %s invoked with: %d\n", __FUNCTION__, b);
	*a = 15;	
	return S_OK;
}

HRESULT CDummyObj::output_args_char(signed char * a)
{
	printf("DLL: %s invoked with: %s\n", __FUNCTION__, a);

	a[0] = 's';
	a[1] = 'o';
	a[2] = 'm';
	a[3] = 'e';
	a[4] = ' ';
	a[5] = 't';
	a[6] = 'e';
	a[7] = 'x';
	a[8] = 't';
	a[9] = '\0';

	return S_OK;
}

HRESULT CDummyObj::output_args_float(/*[in, out]*/ float* f)
{
	printf("DLL: %s invoked with: %.3f\n", __FUNCTION__, *f);
	*f = 12.12f;
	return S_OK;
}

HRESULT CDummyObj::output_args_double(/*[in, out]*/ double * d)
{
	printf("DLL: %s invoked with: %.3f\n", __FUNCTION__, *d);
	*d = 56.12f;
	return S_OK;
}

HRESULT CDummyObj::output_args_bool(boolean /*bool*/ *b, int* result)
{
	printf("DLL: %s invoked with: %s\n", __FUNCTION__, *b ? "true" : "false");
	*b = false;
	*result = 2;
	return S_OK;
}

HRESULT CDummyObj::output_args_int_bool_float(int * i, BOOL * b, float * f, int* result)
{
	printf("DLL: %s invoked with: %d, %s, and %.3f\n", __FUNCTION__, *i, *b ? "true" : "false", *f);
	*i = 12;
	*b = false;
	*f = 3.14f;
	*result = 3;
	return S_OK;
}


HRESULT CDummyObj::input_int_double_output_bool_int_input_string(int inI, float inD, BOOL * b, int * i, const signed char * inC, int* result)
{
	printf("DLL: %s invoked with: %d, %.3f, %s, %d, %s", __FUNCTION__, inI, inD, *b ? "true" : "false", *i, inC);
	*b = false;
	*i = 432;
	*result = 8;
	return S_OK;
}


HRESULT __stdcall CDummyObj::EnableLog( long nEnable)
    {
    if (nEnable)
        m_bIsLogEnabled=TRUE;
    else
        m_bIsLogEnabled=FALSE;

    return S_OK;
    }

HRESULT __stdcall CDummyObj::IsEnabled( long *pEnabled)
    {
    *pEnabled = m_bIsLogEnabled;
    return S_OK;
    }


HRESULT __stdcall CDummyObj::QueryInterface(
                                    REFIID riid , 
                                    void **ppObj)
    {
    if (riid == IID_IUnknown)
        {
	    *ppObj = static_cast<IDummy*>(this) ; 
        AddRef() ;
        return S_OK;
        }

    if (riid == IID_IDummy)
	    {
	    *ppObj = static_cast<IDummy*>(this) ;
        AddRef() ;
        return S_OK;
	    }

    if (riid == IID_IFileIO)
	    {
	    *ppObj = static_cast<IFileIO*>(this) ;
        AddRef() ;
        return S_OK;
	    }

    //
    //if control reaches here then , let the client know that
    //we do not satisfy the required interface
    //

    *ppObj = NULL ;
    return E_NOINTERFACE ;
    }

void  CDummyObj::WriteToLog(char *szBuffer)
    {
    //
    //write to the log file
    //
    /*
    FILE *fp=NULL;
    fp = fopen("
    YOU WERE HERE
    */
    }