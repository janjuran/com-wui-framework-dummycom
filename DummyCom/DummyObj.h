//
//AddObj.h
//Contains the C++ class declarations for implementing the IDummy, IFileIO
//interfaces
//
//#include    "IDummy.h"
#include    "IDummy_h.h"
extern long g_nComObjsInUse;

typedef unsigned char Boolean;
//typedef signed char Boolean;

class CDummyObj : 
        public IDummy , 
        public IFileIO
    {
    public:

    //IUnknown methods are implemented here
    HRESULT __stdcall QueryInterface(
                                REFIID riid , 
                                void **ppObj);


    ULONG   __stdcall AddRef()
        {
        return InterlockedIncrement(&m_nRefCount) ;
        }

    ULONG   __stdcall Release()
        {     
        long nRefCount=0;
        nRefCount=InterlockedDecrement(&m_nRefCount) ;
        if (nRefCount == 0) delete this;
        return nRefCount;
        }


    //IDummy interface    
	HRESULT __stdcall CallWithEmpty();
	HRESULT __stdcall CallWithInt(int i);
	HRESULT __stdcall CallWithBool(BOOL start);
	HRESULT __stdcall CallWithFloat(float f);
	HRESULT __stdcall CallWithDouble(double d);
	HRESULT __stdcall CallWithString(const signed char *project);
	HRESULT __stdcall ReturnInt(/*[out, retval]*/ int* result);
	HRESULT __stdcall ReturnBool(/*[out, retval]*/ BOOL* result);
	HRESULT __stdcall ReturnFloat(/*[out, retval]*/ float* result);
	HRESULT __stdcall ReturnDouble(/*[out, retval]*/ double* result);

	// to be supported
	//EXPORT char *ReturnString();
	HRESULT __stdcall int_func_bool_int(BOOL b, int i, /*[out, retval]*/ int* result);
	HRESULT	__stdcall int_func_int_bool(int i, BOOL b, /*[out, retval]*/ int* result);
	HRESULT	__stdcall add_input_ints(int a, int b, /*[out, retval]*/ int* result);
	HRESULT	__stdcall output_args(/*[in, out]*/ int *a);
	HRESULT	__stdcall output_args_char(/*[in, out]*/ signed char *a);
	HRESULT	__stdcall output_args_float(/*[in, out]*/ float* f);
	HRESULT	__stdcall output_args_double(/*[in, out]*/ double *d);
	//HRESULT	__stdcall output_args_bool(/*[in, out]*/ Boolean *b, /*[out, retval]*/ int* result);
	//HRESULT	__stdcall output_args_bool(/*[in, out]*/ bool *b, /*[out, retval]*/ int* result);
	HRESULT	__stdcall output_args_bool(/*[in, out]*/ boolean *b, /*[out, retval]*/ int* result);
	HRESULT	__stdcall output_args_int_bool_float(/*[int, out]*/ int *i, /*[int, out]*/ BOOL *b, /*[int, out]*/ float *f, /*[out, retval]*/ int* result);
	HRESULT	__stdcall input_int_double_output_bool_int_input_string(/*[in]*/ int inI, /*[in]*/ float inD, /*[out]*/ BOOL *b, /*[out]*/ int *i, /*[in]*/ const signed char *inC, /*[out, retval]*/ int* result);


    //IFileIO interface
    HRESULT __stdcall EnableLog( long nEnable);
    HRESULT __stdcall IsEnabled( long *pEnabled);


    

    CDummyObj()
        {
        //
        //constructor
        //

        m_nRefCount=0;
        m_bIsLogEnabled=FALSE;

        //
        //increment the global count
        //
        InterlockedIncrement(&g_nComObjsInUse);
        
        //
        //set the path to the log file
        //
        GetTempPath(sizeof(szLogPath),szLogPath);
        lstrcat(szLogPath,"cguruAdd.log");
        }


    ~CDummyObj()
        {
        //
        //destructor
        //decerement the global object count
        InterlockedDecrement(&g_nComObjsInUse);
            
        }



    
    private:
    void WriteToLog(char *szBuffer);
	  

    long m_nRefCount;   //for managing the reference count
    char m_szLogFile[260];
    BOOL m_bIsLogEnabled;
    char szLogPath[MAX_PATH];

    };